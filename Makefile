

all : fetch csvs push
.PHONY : all

fetch:
	python2 ./Scripts/gpsync.py synccurrent

csvs:
	python2 ./Scripts/gpsync.py sync_csv | R --vanilla 

sqlite: Data/SQLite/gpinhours.sqlite

CSVS=$(wildcard Data/CSV/*.csv)
Data/SQLite/gpinhours.sqlite: $(CSVS)
	R --vanilla < Scripts/buildsql.R

push:
	git add ./Data
	git commit -a -m 'New push'
	git push
