#!/bin/env python
from bs4 import BeautifulSoup
import urllib2
import os.path 
import os
import urllib
import re

SITE="https://www.gov.uk/"
#INDEX=SITE+"/government/publications/gp-in-hours-weekly-bulletins-for-2018/"
INDEX = "https://www.gov.uk/government/publications/gp-in-hours-weekly-bulletins-for-2021"

ARCHIVE={
    '2014': "http://webarchive.nationalarchives.gov.uk/20140722091854/http://www.hpa.org.uk/Topics/InfectiousDiseases/InfectionsAZ/RealtimeSyndromicSurveillance/SyndromicSystemsAndBulletinArchive/GPInhoursunscheduledCareSurveillanceSystem/primcGPinhoursscheduledcareSurvSysBulletinArchive2014/",
    '2013': "http://webarchive.nationalarchives.gov.uk/20140722091854/http://www.hpa.org.uk/Topics/InfectiousDiseases/InfectionsAZ/RealtimeSyndromicSurveillance/SyndromicSystemsAndBulletinArchive/GPInhoursunscheduledCareSurveillanceSystem/primcGPinhoursscheduledcareSurvSysBulletinArchive2013/"
}


HERE = os.getcwd()
XLSDIR = os.path.join(HERE,"Data","XLS")
CSVDIR =  os.path.join(HERE,"Data","CSV")

def getsoupindex(url):
    html = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html,"lxml")
    return(soup)

def getXLS(soup):
    links = [x['href'] for x in soup.findAll("a",{'class': 'thumbnail'})]
    links = filter(lambda x: x[-4:]=='.xls' or x[-4:]=='.ods', links)
    return links

def missing(links):
    return filter(doesnt_exist, links)

def check_doesnt_existor(path):
    def check_doesnt_exist(link):
        filename = os.path.basename(link)
        return not os.path.isfile(os.path.join(path,filename))
    return check_doesnt_exist

def doesnt_exist(link):
    filename = os.path.basename(link)
    return not os.path.isfile(os.path.join(XLSDIR,filename))

def syncarchive(year):
    print "syncing ",year
    html = urllib2.urlopen(ARCHIVE[year]).read()
    soup = BeautifulSoup(html)
    return soup

def synccurrent():
    soup = getsoupindex(INDEX)
    links = getXLS(soup)
    xlslinks = missing(links)

    for xls in xlslinks:
        print "Getting ",xls
        path = os.path.join(XLSDIR,os.path.basename(xls))
        url = xls
        urllib.urlretrieve(url, path)

def noext(files):
    return [os.path.splitext(f)[0] for f in files]

def fnord():
    print "fnord"

def sync_csv_ext(extDIR, CSVDIR, ext):
    src_files = set(noext([f for f in os.listdir(extDIR) if re.match(r'.*\.%s' % ext, f)]))

    csv_files = set(noext(os.listdir(CSVDIR)))
    
    missing_csvs = src_files - csv_files
    missing_srcs = csv_files - src_files

    if len(missing_csvs)>0 :
        print "library(devtools)"
        print "load_all('./gpinhours')"
        for stub in missing_csvs:
            print "# convert ", os.path.join(XLSDIR, stub+"."+ext)," to  ",os.path.join(CSVDIR, stub+".csv")
            print "tocsv('%s','%s')" % (os.path.join(XLSDIR, stub+"."+ext),os.path.join(CSVDIR, stub+".csv"))

    if len(missing_srcs) > 0:
        print "# Warning, some CSV with no  source"
        # print [x+"."+ext for x in missing_srcs]
    
    
def sync_csv():
    sync_csv_ext(XLSDIR, CSVDIR, "ods")
    sync_csv_ext(XLSDIR, CSVDIR, "xls")
    return
    xls_files = set(noext(os.listdir(XLSDIR)))
    csv_files = set(noext(os.listdir(CSVDIR)))
    
    missing_csvs = xls_files - csv_files
    missing_xlss = csv_files - xls_files

    if len(missing_csvs)>0 :
        print "library(devtools)"
        print "load_all('./gpinhours')"
        for stub in missing_csvs:
            print "# convert ", os.path.join(XLSDIR, stub+".xls")," to  ",os.path.join(CSVDIR, stub+".csv")
            print "tocsv('%s','%s')" % (os.path.join(XLSDIR, stub+".xls"),os.path.join(CSVDIR, stub+".csv"))

    if len(missing_xlss) > 0:
        print "Warning, some CSV with no XLS source"
        print [x+".xls" for x in missing_xlss]

def main(args):
    if args[1]=="synccurrent":
        synccurrent()
    elif args[1]=="sync_csv":
        sync_csv()
    else:
        raise ValueError,"Bad arg"
        
if __name__=="__main__":
    import sys
    main(sys.argv)
