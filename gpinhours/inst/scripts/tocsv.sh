#!/bin/bash

export MYLIBDIR=`dirname $0`/../../

R --slave --args "$@" <<EOF >/dev/null 2>&1
require(devtools)
L = Sys.getenv("MYLIBDIR")
load_all(L)
argv = commandArgs(trailingOnly=TRUE)
tocsv(argv[1], argv[2])

EOF

