read_mat <- function(travelfile, testcolumn=23){

    colcodes = data.table::fread(travelfile,skip=9,nrows=2,head=FALSE)
    colNAME = unlist(colcodes[1,-c(1:2)])
    colCODE = unlist(colcodes[2,-c(1:2)])

    travelmat = data.table::fread(travelfile,skip=11,fill=TRUE)

    ## use column 23 as a marker to see where the data ends
    travelmat = travelmat[-which(is.na(travelmat[,..testcolumn])),]
    rowNAME = travelmat$V1
    rowCODE = travelmat$V2

    ## rows are workplaces, columns are homes
    travelmat = as.matrix(travelmat[,-c(1,2)])
    dimnames(travelmat) = list(rowNAME, colNAME)
    list(residence = data.frame(CODE=colCODE, NAME=colNAME),
         workplace = data.frame(CODE=rowCODE, NAME=rowNAME), 
         homework = travelmat
         )
}


merge_mat <- function(mat, inputd, outputd){
    rows = match(inputd$NAME,mat$workplace$NAME)
    rows
}

test_mat <- function(){
    workplaces = c(LETTERS[1:4],"Z")
    homes = LETTERS[1:4]
    mat = matrix(1:(length(workplaces)*length(homes)), length(workplaces), length(homes))
    list(residence = data.frame(CODE=homes, NAME=tolower(homes)),
         workplace = data.frame(CODE=workplaces, NAME=tolower(workplaces)),
         homework=mat
         )
}
